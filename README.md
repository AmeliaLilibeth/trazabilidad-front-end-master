# trazabilidad-front-end

## Rutas

Tomar como referencia la ruta de ingresar
Para funcionalidad basada en la ruta trabajar con la propiedad "name"

## Vistas

Se puede tomar como referencia la vista Home.vue
Para trabajar de una mejor manera se puede encerrar las vistas en contenedores
Ej.

```
<v-container>
  <v-row>
    <v-col>
      //Contenido
    </v-col>
  </v-row>
</v-container>
```

Más referencia en https://vuetifyjs.com/en/getting-started/quick-start/

## Navegación

El menu lateral se encuentra en components
Para agregar mas botones/rutas, se deben agregar en la data
Igual que los ejemplos ya planteados

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
